<?php

require __DIR__ .'/../vendor/autoload.php';

use \App\Utils\View;

//DEFINE A CONSTANTE DA URL
define('URL', 'http://127.0.0.1/shop');

//DEFINE VALOR PADRAO DAS VARIAVEIS
View::init([
    'URL' => URL
]);