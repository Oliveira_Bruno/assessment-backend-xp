<?php

require __DIR__.'/includes/app.php';

use \App\Http\Router;

//INICIA ROUTER
$obRouter = new Router(URL);

//INCLUI AS ROTAS DE PAGINAS
include __DIR__.'/routes/pages.php';

//IMPRIME O RESPONSE DA PAGINA
$obRouter->run()->sendResponse();
