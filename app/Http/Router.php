<?php

namespace App\Http;
use \Closure;
use \Exception;
use \ReflectionFunction;

class Router{

    /**
     * Url completa do projeto (raiz)
     * @var string
     */
    private $url = ''; 

    /**
     * prefixo de todas as rotas
     * @var string
     */
    private $prefix = '';

    /**
     * indice de rotas
     * @var array
     */
    private $routes = [];

    /**
     * instancia de request
     * @var Request
     */
    private $request;

    /**
     * metodo responsavel por iniciar a classe
     */
    public function __construct($url){
        $this->request = new Request($this);
        $this->url = $url;
        $this->setPrefix();
    }

    /**
     * metodo responsavel por definir os prefixos das rotas
     */
    private function setPrefix(){
        $parseUrl = parse_url($this->url);
        $this->prefix = $parseUrl['path'] ?? '';
    }

    /**
     * metodo responsavel por adicionar uma rota na classe
     * @param string $method
     * @param string $route
     * @param array $params
     */
    private function addRoute($method, $route, $params = []){
        foreach($params as $key=>$value){
            if($value instanceof Closure){
                $params['controller'] = $value;
                unset($params[$key]);
                continue;
            }
        }
        //VARIAVEIS DA ROTA
        $params['variables'] = [];

        //PADRAO DE VALIDAÇÃO DAS VARIAVEIS DE ROTAS
        $patternVariable = '/{(.*?)}/';
        if(preg_match_all($patternVariable,$route,$matches)){
            $route = preg_replace($patternVariable,'(.*?)',$route);  
            $params['variables'] = $matches[1];
        }

        //PADRAO DE VALIDAÇÃO DA URL
        $patternRoute = '/^'.str_replace('/','\/',$route).'$/';
       
        

        $this->routes[$patternRoute][$method] = $params; 
    }

    /**
     * metodo responsavel por definir uma rota GET
     * @param string $route
     * @param array $params
     */
    public function get($route, $params = []){
        return $this->addRoute('GET',$route,$params);
    }

     /**
     * metodo responsavel por definir uma rota POST
     * @param string $route
     * @param array $params
     */
    public function post($route, $params = []){
        return $this->addRoute('POST',$route,$params);
    }

    /**
     * metodo responsavel por definir uma rota PUT
     * @param string $route
     * @param array $params
     */
    public function put($route, $params = []){
        return $this->addRoute('PUT',$route,$params);
    }

    /**
     * metodo responsavel por definir uma rota DELETE
     * @param string $route
     * @param array $params
     */
    public function delete($route, $params = []){
        return $this->addRoute('DELETE',$route,$params);
    }

    /**
     * metodo responsavel por retornar a uri desconsiderando o prefixo
     * @return string
     */
    public function getUri(){
        $uri = $this->request->getUri();

        //FATIA A URI COM O PREFIXO
        $xUri = strlen($this->prefix) ? explode($this->prefix,$uri) : [$uri];
        return end($xUri);
    }

    /**
     * metodo responsavel por retornar os dados da rota atual
     * @return array
     */
    private function getRoute(){
        
        //URI
        $uri = $this->getUri();

        //METHOD
        $httpMethod = $this->request->getHttpMethod();

        //VALIDA AS ROTAS
        foreach($this->routes as $patternRoute=>$methods){
            //VERIFICA SE A URI BATE COM O PADRAO DE ROTAS E RETORNA A ROTA
            if(preg_match($patternRoute,$uri,$matches)){
                //VERIFICA O METODO
                if(isset($methods[$httpMethod])){
                    //REMOVE A PRIMEIRA POSIÇÃO
                    unset($matches[0]);

                    //VARIAVEIS PROCESSADAS
                    $keys = $methods[$httpMethod]['variables'];
                    $methods[$httpMethod]['variables'] = array_combine($keys,$matches);
                    $methods[$httpMethod]['variables']['request'] = $this->request;
                    

                    //RETORNA PARAMETROS DA ROTA
                    return $methods[$httpMethod];
                }
                throw new Exception("Método não permitido", 405);
            }

            /*echo "<pre>";
            print_r($patternRoute);
            echo "</pre>";*/

        }
       // URL NAO ENCONTRADA
        throw new Exception("Url não encontrada", 404);
    }

    /**
     * metodo responsavel por executar a rota atual
     * @return Response
     */
    public function run(){
        try{
            //OBTEM A ROTA ATUAL
           $route = $this->getRoute();
           

           //VERIFICA O CONTROLADOR
           if(!isset($route['controller'])){
               throw new Exception("Url não pode ser processada", 500);
           }

           $args = [];

           //REFLECTION
           $reflection = new ReflectionFunction($route['controller']);
           foreach($reflection->getParameters() as $parameter){
            
            $name = $parameter->getName();
           
            $args[$name] = $route['variables'][$name] ?? '';
           }
          
           return call_user_func_array($route['controller'], $args);
           
        }
        catch(Exception $e){
            return new Response($e->getCode(),$e->getMessage());
        }
    }
    
}