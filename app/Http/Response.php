<?php

namespace App\Http;

class Response{

    /**
     * Código do status Http
     * @var integer
     */
    private $httpCode = 200;

    /**
     * Cabeçalho do response
     * @var array
     */
    private $headers = [];

    /**
     * tipo de conteudo que esta sendo retornado
     * @var string
     */
    private $contentType = 'text/html';

    /**
     * conteudo response
     * @var mixed
     */
    private $content;

    /**
     * Metodo responsavel por iniciar a classe e definir os valores
     * @param integer
     * @param mixed
     * @param string
    */
    public function __construct($httpCode, $content, $contentType = 'text/html'){
        $this->httpCode = $httpCode;
        $this->content = $content;
        $this->setContentType($contentType);
    }

    /**
     * metodo responsavel por alterar o content type do response
     * @param string
     */
    public function setContentType($contentType){
        $this->contentType = $contentType;
        $this->addHeader('Content-type', $contentType);
    }

    /**
     * Metodo responsavel por adicionar um registro no cabeçalho de response
     * @param string $key
     * @param string $value
     */
    public function addHeader($key, $value){
        $this->header[$key] = $value;
    }

     /**
     *Metodo responsavel por enviar os headers para o navegador 
     */
    private function sendHeaders(){
        http_response_code($this->httpCode);
        foreach($this->headers as $key=>$value){
            header($key.': '.$value);
        }
    }

    /**
     * Metodo responsavel por enviar a resposta para o usuario
     */
    public function sendResponse(){
        $this->sendHeaders();
        switch ($this->contentType){
            case 'text/html':
                echo $this->content;
                exit;
        }
    }
   
}
