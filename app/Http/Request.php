<?php

namespace App\Http;

class Request{

    /**
     * Instancia do router
     * @var Router
     */
    private $router;

    /**
     * Método HTTP da requisição
     * @var string
     */
    private $httpMethod;

    /**
     * Uri da pagina
     * @var string
     */
    private $uri;

    /**
     * Parametros da Url ($_GET)
     * @var array
     */
    private $queryParams = [];

    /**
     * variaveis recebinas no metodo POST
     * @var array
     */
    private $postVars = [];

    /**
     * cabeçalho da requisição
     */
    private $headers = [];

    /**
     * construtor da classe
     */
    public function __construct($router){
        $this->router      = $router;
        $this->queryParams = $_GET ?? [];
        $this->postVars    = $_POST ?? [];
        $this->headers     = getallheaders(); 
        $this->httpMethod  = $_SERVER['REQUEST_METHOD'] ?? '';
        $this->setUri();
    }

    /**
     * Metodo responsavel por definir a URI
     */
    private function setUri(){
        //URI COMPLETA
        $this->uri = $_SERVER['REQUEST_URI'] ?? '';

        //REMOVE GETS DA URI
        $xUri = explode ('?',$this->uri);
        $this->uri = $xUri[0];
    }

    /**
     * Metodo responsavel por retornar a instancia do Router
     * @return router
     */
    public function getRouter(){
        return $this->router;
    }

    /**
     * metodo responsavel por retornar o metodo http da requisição
     * @return string
     */
    public function getHttpMethod(){
        return $this->httpMethod;
    }

    /**
     * metodo responsavel por retornar a uri da requisição
     * @return string
     */
    public function getUri(){
        return $this->uri;
    }

    /**
     * metodo responsavel por retornar os headers da requisição
     * @return array
     */
    public function getHeaders(){
        return $this->headers;
    }

    /**
     * metodo responsavel por retornar as variaveis post da requisição
     * @return array
     */
    public function getPostVars(){
        return $this->postVars;
    }

    /**
     * metodo responsavel por retornar os parametros da requisição
     * @return array
     */
    public function getQueryParams(){
        return $this->queryParams;
    } 
}