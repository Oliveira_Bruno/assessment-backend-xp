<?php

namespace App\model\entities;
use \App\Db\Database;
use Exception;

class Product{

    public $id_product;
    public $sku_product;
    public $price_product;
    public $name_product;
    public $description_product;
    public $quantity_product;

    /**
     * Metodo responsavel por cadastrar um novo produto e syuas categorias no banco de dados
     * @return boolean
     */
    public function saveProduct(){
        try{      
            $objDatabase = new Database('product');
                $this->id = $objDatabase->insert([
                    'sku_product'         => $this->sku,
                    'name_product'        => $this->name,
                    'price_product'       => $this->price,
                    'description_product' => $this->description,
                    'quantity_product'    => $this->quantity
                ]);
                
                $objDatabase = new Database('product_category');
                $params = $this->category; 
                
                foreach($params as $value){
                    $objDatabase->insert([
                    'id_product'  => $this->id,
                    'id_category' => $value
                    ]);
                }
            ActionLogs::logMsg( "Produto: ".$this->name." inserida com sucesso", 'info' );
            return true;
        }
        catch(Exception $e){
            ActionLogs::logMsg( "Erro: ".$e->getMessage(), 'error' );
        }
    }
    /**
     * Metodo responsavel por obter todos os produtos do banco de dados
     * @param string $where
     * @param string $order
     * @param string $limit
     * @param string $fields
     * @return PDOStatement
     */
    public static function getAllProducts($where = null, $order = null, $limit = null, $fields = '*')
    {
        return (new Database('product'))->select($where, $order, $limit, $fields);
    }
    public static function getAmountProducts($field = null){
        return (new Database('product'))->count($field); 
    }

    /**
     * Metodo responsavel por buscar um produto com base em seu id
     * @param integer $id
     * @return Product
     */
    public static function getProductById($id){
        return (new Database('product'))->select('id_product = '.$id)
                                        ->fetchObject(self::class);
    }

     /**
     * Metodo responsavel por atualizar um produto
     * @return boolean
     */
    public function updateProduct($idProduct){
        ActionLogs::logMsg( "Produto: ".$this->$idProduct." Atualizado com sucesso", 'info' );
       return (new Database('product'))->update('id_product = '.$idProduct, [
                            'sku_product'         => $this->sku,
                            'name_product'        => $this->name,
                            'price_product'       => $this->price,
                            'description_product' => $this->description,
                            'quantity_product'    => $this->quantity
                        ]);    
    }

    /**
     * Metodo responsavel por deletar um produto
     * @return boolean
     */
    public function deleteProduct($idProduct){
        //DELETA DA TABELA INTERMEDIARIA
        (new Database('product_category'))->delete('id_product = '.$idProduct);
        //DELETA DA TABELA PRODUTO
        (new Database('product'))->delete('id_product = '.$idProduct);

        ActionLogs::logMsg( "Produto: ".$this->$idProduct." Deletado", 'info' );
        return true;
    }

    /**
     * Metodo responsavel por fazer a junção das tabelas produto e categoria
     * 
     */
    public static function getProductWithCategory($id){
        return (new Database('product'))->getProductAndCategory($id);
    }

}
