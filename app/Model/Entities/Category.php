<?php
/**
 * Classe Products com atributos e funções
 * @author bruno de Souza Oliveira
 */
namespace App\model\entities;

use \App\Db\Database;

class Category{
    /**
     * Unique Id
     * @var Integer
     */
    public $id;

    /**
     * @var String
     */
    public $name;

     /**
     * Metodo responsavel por persistir os dados no banco.
     * @return boolean
     */
    public function saveCategory(){
        $objDatabase = new Database('category');
        $this->id = $objDatabase->insert([
                        'name_category'=> $this->name,
                        'id_category'=> $this->code                                    
                        ]);
        ActionLogs::logMsg( "Categoria: ".$this->name." inserida com sucesso", 'info' );                
        return true;
        
    }

     /**
     * Metodo responsavel por obter as categorias do banco de dados
     * @param string $where
     * @param string $order
     * @param string $limit
     * @param string $fields
     * @return PDOStatement
     */
    public static function getAllCategories($where = null, $order = null, $limit = null, $fields = '*'){
        return (new Database('category'))->select($where, $order, $limit, $fields);
    }
    
}