<?php

namespace App\Controller\Pages;

use \App\Utils\View;
use App\model\entities\Category;
use \App\model\entities\Product;

class ProductController extends Page{

    /**
     * Metodo responsavel por obter a renderização dos itens de categorias para popular o select
     * @return String
     */
    private static function getCategoryList(){
        //PRODUTOS
        $categories = '';

        //FAZ A BUSCA NO BANCO DE DADOS
        $results = Category::getAllCategories();

        //RENDERIZA OS ITENS 
        while($obCategory = $results->fetchObject(Category::class)){    

            $categories .= View::render('pages/product/categoriesOption',[ 
                'name' => $obCategory->name_category,
                'id' => $obCategory->id_category
            ]);
        }
        //RETORNA  AS CATEGORIAS
        return $categories;        
    }  

    /**
     * Metodo responsavel por retornar o conteudo (view) da pagina html formAddProduct
     * @return string
     */
    public static function getFormAddProduct(){
        $content = View::render('pages/product/formAddProduct',[
            'options' => self::getCategoryList()         
        ]);
        //RETORNA A VIEW DA PAGINA
        return parent::getPage('Add new product',$content);
    }

    /**
     * metodo responsavel por cadastrar um produto
     * @param $request
     * @return string
     */
    public static function insertProduct($request){
        $request->getPostVars();
 
        $obProduct = new Product;

        $obProduct->sku =          $_POST['sku'];
        $obProduct->price =        $_POST['price'];
        $obProduct->name =         $_POST['name'];
        $obProduct->description =  $_POST['description'];
        $obProduct->quantity =     $_POST['quantity'];
        $obProduct->category =     $_POST['category'];
 
        $obProduct->saveProduct();

        return self::getFormAddProduct();    
    }

    /**
     * Metodo responsavel por obter a renderização dos itens de produtos
     * @return String
     */
    public static function getProductList(){
        //PRODUTOS
        $itens = '';

        //RESULTADOS DA PAGINA
        $results = Product::getAllProducts(null,'id_product DESC');

        //RENDERIZA OS ITENS 
        while($obProduct = $results->fetchObject(Product::class)){    
            $itens .= View::render('pages/product/productList',[ 
                'id'=> $obProduct->id_product,
                'name' => $obProduct->name_product,
                'sku' => $obProduct->sku_product,
                'quantity' => $obProduct->quantity_product,
                'price' => $obProduct->price_product,
                'productHasCategories' => self::getProductHasCategory($obProduct->id_product)     
            ]);      
        }
        //RETORNA OS PRODUTOS
        return $itens;        
    } 

    /**
     * Metodo responsavel por obter a renderização das categorias de cada produto
     * @return String
     */
    public static function getProductHasCategory($id_product){
        //CATEGORY
        $itens = '';
        
        //RESULTADOS DA PAGINA
        $results = Product::getProductWithCategory($id_product);
        
        //RENDERIZA OS ITENS 
        while($obProduct = $results->fetchObject(Product::class)){            
            $itens .= View::render('pages/product/productHasCategories',[ 
                'category'=> $obProduct->name_category
            ]);      
        }
        //RETORNA OS PRODUTOS
        return $itens;        
    } 

    /**
     * Metodo responsavel por excluir um produto a partir do id informado
     * @param string $idProduct
    */
    public static function deleteProductById($idProduct){
        $obProduct = new Product;
       
        $obProduct->deleteProduct($idProduct);

        return self::getProduct();
    }

     /**
     * Metodo responsavel por atualizar um produto a partir do seu Id 
     * @param $request
     * @param string $idProduct
    */
    public static function updateProduct($request, $idProduct){
        $request->getPostVars();
        $obProduct = new Product;
        $obProduct->sku =          $_POST['sku'];
        $obProduct->price =        $_POST['price'];
        $obProduct->name =         $_POST['name'];
        $obProduct->description =  $_POST['description'];
        $obProduct->quantity =     $_POST['quantity'];
        $obProduct->category =     $_POST['category'];
        $obProduct->updateProduct($idProduct);

        return self::getProduct();
    }

     /**
     * Metodo responsavel por editar um produto a partir do id informado
     * @param $productId
     * @return String
     */
    public static function getFormEditProductById($productId){
        $content = '';
        $obProduct = Product::getProductById($productId);

        $content .= View::render('pages/product/formEditProduct',[
            'name'        => $obProduct->name_product,
            'sku'         => $obProduct->sku_product,
            'quantity'    => $obProduct->quantity_product,
            'price'       => $obProduct->price_product,
            'description' => $obProduct->description_product,
            'options'     => self::getCategoryList()     
        ]);

        return parent::getPage('Edit product',$content);
    }

     /**
     * Metodo responsavel por retornar o conteudo (view) da pagina html product
     * @return string
     */
    public static function getProduct(){
        $content = View::render('pages/product',[
            'itens' => self::getProductList()        
        ]);
        return parent::getPage('WebJump | Backend Test | products',$content);
    }    
}