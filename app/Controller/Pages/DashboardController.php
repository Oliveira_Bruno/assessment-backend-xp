<?php

namespace App\Controller\Pages;

use App\model\entities\Product;
use \App\Utils\View;

class DashboardController extends Page{

    /**
     * Metodo responsavel por obter a renderização dos itens de produtos
     * @return String
     */
    public static function getProductList(){
        //PRODUTOS
        $itens = '';

        //FAZ A BUSCA NO BANCO DE DADOS
        $results = Product::getAllProducts(null,'id_product DESC');

        //RENDERIZA OS ITENS 
        while($obProduct = $results->fetchObject(Product::class)){    

        $itens .= View::render('pages/product/productListDashboard',[ 
            'name' => $obProduct->name_product,
            'quantity' => $obProduct->quantity_product,
            'price' => $obProduct->price_product, 
            'avaiable' => self::verifyIfIsAvaiable($obProduct->quantity_product),     
            ]);
         }
        //RETORNA OS PRODUTOS
        return $itens;        
    }  

    /**
     * Metodo para verificar se um produto tem quantidade maior que zero e setar disponivel ou indisponivel
     * @param $quantity
     * @return string
     */
    private static function verifyIfIsAvaiable($quantity){
        return ( $quantity > 0 ? "avaiable" : "out of stock" );
    }
        
    /**
     * Metodo responsavel por retornar o conteudo (view) da pagina html dashboard
     * @return string
     */
    public static function getDashboard(){
        $content = View::render('pages/dashboard',[
            'totalAmount' => Product::getAmountProducts('\*'),
            'itens' => self::getProductList()        
        ]);
        return parent::getPage('WebJump | Backend Test | Dashboard',$content);
    }    
}
