<?php

namespace App\Controller\Pages;

use \App\Utils\View;
use \App\model\entities\Category;

class CategoryController extends Page{

    /**
     * Metodo responsavel por retornar o conteudo (view) da pagina html addCategory
     * @return string
     */
    public static function getFormAddCategory(){
        $content = View::render('pages/category/formAddCategory',[      
        ]);
        //RETORNA A VIEW DA PAGINA
        return parent::getPage('Add new category',$content);
    }

    /**
     * metodo responsavel por cadastrar um produto
     * @param $request
     * @return string
     */
    public static function insertCategory($request){
        $request->getPostVars();
        $obCategory = new Category;

        $obCategory->name = $_POST['name'];
        $obCategory->code = $_POST['code'];
      
        $obCategory->saveCategory();

        return self::getCategory();
    }

     /**
     * Metodo responsavel por obter a renderização dos itens de categorias
     * @return String
     */
    private static function getCategoryList(){
        //PRODUTOS
        $itens = '';

        //FAZ A BUSCA NO BANCO DE DADOS
        $results = Category::getAllCategories();

        //RENDERIZA OS ITENS 
        while($obCategory = $results->fetchObject(Category::class)){    

        $itens .= View::render('pages/category/categoryList',[ 
            'name' => $obCategory->name_category,
            'code' => $obCategory->id_category
            ]); 
        }
        //RETORNA  AS CATEGORIAS
        return $itens;        
    }  

    /**
     * Metodo responsavel por retornar o conteudo (view) da pagina html category
     * @return string
     */
    public static function getCategory(){
        $content = View::render('pages/category',[
            'itens' => self::getCategoryList()        
        ]);
        return parent::getPage('WebJump | Backend Test | Categories',$content);
    }
}