<?php
namespace App\Utils;

class View{

    /**
     * variaveis padroes da view
     * @array
     */
    private static $vars = [];

    /**
     * metodo responsavel por definir os dados iniciais da classe
     */
    public static function init($vars = []){
        self::$vars = $vars;
    }

     /**
     * Metodo responsavel por retornar o conteudo de uma view
     * @param string $view
     * @return string
     */
    private static function getContentView($view){
        $file = __DIR__.'/../../resources/view/'.$view.'.html';

        //se arquivo existir retornar conteudo, se nao existir retorna vazio
        return file_exists($file) ? file_get_contents($file) : '';
    }

    /**
     * Metodo responsavel por retornar o conteudo renderizado de uma view
     * @param string $view
     * @param array $vars [strings/numeric]
     * @return string
     */
    public static function render($view, $vars = []){
        //CONTEUDO DA VIEW
        $contentView = self::getContentView($view);

        //MERGE DE VARIAVEIS DA VIEW
        $vars = array_merge(self::$vars,$vars);

        //CHAVES DO ARRAY DE VARIAVEIS
        $keys = array_keys($vars);
        $keys = array_map(function($item){
            return '{{'.$item.'}}';
        },$keys);

        //RETORNA CONTEUDO RENDERIZADO
        return str_replace($keys,array_values($vars),$contentView);

    }
}
