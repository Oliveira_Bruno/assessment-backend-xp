<?php
/**
 * Classe para definir a conexão com o banco de dados
 */
namespace App\Db;

use \PDO;
use PDOException;
use \App\model\entities\ActionLogs;

class Database
{

    /**
     * Host de conexão com o banco de dados
     * @var string
     */
    const HOST = 'localhost';

    /**
     *Nome do banco de dados 
     *@var string
     */
    const NAME = 'webjump';

    /**
     *usuario do banco de dados
     *@var string
     */
    const USER = 'root';

    /**
     *Senha de acesso ao banco de dados
     *@var string
     */
    const PASS = '';

    /**
     * Nome da tabela que sera manipulada
     * @var string
     */
    private $table;

    /**
     * Instancia de conexão com banco de dados
     * @var PDO
     */
    private $connection;

    /**
     * Define tabela, instancia e conexão
     * @param string $table
     */
    public function __construct($table = null)
    {
        $this->table = $table;
        $this->setConnection();
    }
    /**
     * Metodo responsavel por criar uma conexão com o banco de dados utilizando PDO
     * @return void
     */
    private function setConnection()
    {
        try {
            $this->connection = new PDO(
                'mysql:host='.self::HOST .
                ';dbname='   .self::NAME,
                self::USER,
                self::PASS
            );
            $this->connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (PDOException $e) {
            ActionLogs::logMsg( "Erro ao conectar com o Bd: ".$e->getmessage(), 'error' );
            die('ERROR AO CONECTAR COM O BANCO DE DADOS'); 
        }
    }
    /**
     * Metodo responsavel por persistir queries
     * @param string $query
     * @param array $params
     * @return PDOStatement
     */
    public function execute($query, $params = [])
    {
        try {
            $statement = $this->connection->prepare($query);
            $statement->execute($params);  
            return $statement;
            
        } catch (PDOException $e) {
            ActionLogs::logMsg( "Erro ao excutar query: ".$e->getmessage(), 'error' );
            die('ERROR AO EXECUTAR QUERY NO BANCO DE DADOS'); 
        }
    }

    /**
     * Metodo responsavel por inserir dados no banco
     * @param $values [ field => value ]
     * @return integer
     */
    public function insert($values)
    {
        //DADOS DA QUERY
        $fields = array_keys($values);
        $fieldsImploded = implode(',', $fields);
        $binds = array_pad([], count($fields), '?');
        $bindsImploded = implode(',', $binds);

        //MONTA A QUERY
        $query = 'INSERT INTO ' . $this->table . ' (' . $fieldsImploded . ') VALUES (' . $bindsImploded . ')';

        //EXECUTA O INSERT
        $this->execute($query,array_values($values));

        //RETORNA O ID INSERIDO
        return $this->connection->lastInsertId();
       
    }

    /**
     * Metodo responsavel por executar uma consulta no banco de dados
     * @param string $where
     * @param string $order
     * @param string $limit
     * @return PDOStatement
     */
    public function select($where = null, $order = null, $limit = null, $fields = '*'){
        //DADOS DA QUERY
        $where = strlen($where) ? 'WHERE '.$where : '';
        $order = strlen($order) ? 'ORDER BY '.$order : '';
        $limit = strlen($limit) ? 'LIMIT '.$limit : '';

        //MONTA QUERY
        $query = 'SELECT '.$fields.' FROM '.$this->table.' '.$where.' '.$order.' '.$limit;

        //EXECUTA A QUERY
        return $this->execute($query);
    }

    /**
     * Metodo responsavel por executar uma contagem no banco de dados
     * @param string $field
     * @return string $result
     */
    public function count($field){
        //MONTA QUERY
        $query = 'SELECT COUNT("\*") FROM '.$this->table;
        //EXECUTA A QUERY
        $result = $this->execute($query)->fetchColumn();  
   
         return $result; 
        
    }

    /**
     * metodo responsavel por executar atualizações no banco de dados
     * @param string $where
     * @param array $values [ field => value ]
     * @return boolean
     */
    public function update($where,$values){
        //DADOS DA QUERY
        $fields = array_keys($values);

        //MONTA A QUERY
        $query = 'UPDATE '.$this->table.' SET '.implode('=?,',$fields).'=? WHERE '.$where;
       

        //EXECUTA O INSERT
        $this->execute($query,array_values($values));
    }

    /**
     * Metodo responsavel por deletar registro no banco de dados
     * @param String $where
     * @return boolean
     */
    public function delete($where){
        //MONTA A QUERY
        $query = 'DELETE FROM '.$this->table.' WHERE '.$where;

        //EXECUTA A QUERY
        $this->execute($query);

        return true;
    }

     /**
     * Metodo responsavel por consultar produtos com suas categorias no banco de dados
     * @param String $where
     * @return PDOStatement
     */
    public function getProductAndCategory($id_product){
        //MONTA A QUERY
        $query = 
            'SELECT category.name_category       
                FROM product INNER JOIN product_category
                ON product.id_product = product_category.id_product
                INNER JOIN category
                ON category.id_category = product_category.id_category
                where product.id_product = '.$id_product;


        //EXECUTA A QUERY
       return $this->execute($query);

    }
}
