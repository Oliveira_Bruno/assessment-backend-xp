<?php
/**
 * Classe responsavel por determinar as rotas
 * @autor Bruno de Souza Oliveira
 */
 use \App\Http\Response;
 use \App\Controller\Pages;

 //ROTA DINAMICA
$obRouter->get('/pagina/{idPagina}/{acao}',[
    function($idPagina,$acao){
        return new Response(200, 'pagina '.$idPagina.' - '.$acao);
    }
]);

 //ROTA DASHBOARD PAGINA PRINCIPAL
$obRouter->get('/',[
    function(){
        return new Response(200, Pages\DashboardController::getDashboard());
    }
]);

 //ROTA PRODUCTS MOSTRAR LISTA DE PRODUTOS 
 $obRouter->get('/products',[
    function(){
        return new Response(200, Pages\ProductController::getProduct());
    }
]);

 //ROTA FORMULARIO PARA ADICIONAR NOVO PRODUTO
 $obRouter->get('/formAddProduct',[
    function(){
        return new Response(200, Pages\ProductController::getFormAddProduct());
    }
]);

 //ROTA ADD PRODUCT (POST)
 $obRouter->post('/formAddProduct',[
    function($request){      
        return new Response(200, Pages\ProductController::insertProduct($request));

    }
]);

//ROTA DINAMICA PARA DELETAR PRODUTO
$obRouter->get('/deleteProduct/id={idProduct}',[
    function($idProduct){
        return new Response(200, Pages\ProductController::deleteProductById($idProduct));
    }
]);

//ROTA FORM DINAMICA PARA EDITAR PRODUTO
$obRouter->get('/editProduct/id={idProduct}',[
    function($idProduct){
        return new Response(200, Pages\ProductController::getFormEditProductById($idProduct));
    }
]);

//ROTA DINAMICA PARA EDITAR PRODUTO (POST)
$obRouter->post('/editProduct/id={idProduct}',[
    function($request, $idProduct){      
        return new Response(200, Pages\ProductController::updateProduct($request, $idProduct));
    }
]);

 //ROTA CATEGORY MOSTRAR LISTA DE CATEGORIAS 
 $obRouter->get('/categories',[
    function(){
        return new Response(200, Pages\CategoryController::getCategory());
    }
]);

 //ROTA FORMULARIO PARA ADICIONAR NOVA CATEGORIA
 $obRouter->get('/formAddCategory',[
    function(){
        return new Response(200, Pages\CategoryController::getFormAddCategory());
    }
]);

 //ROTA ADD CATEGORY (POST)
 $obRouter->post('/formAddCategory',[
    function($request){       
        return new Response(200, Pages\CategoryController::insertCategory($request));

    }
]);